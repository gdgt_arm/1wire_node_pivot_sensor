#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <string.h>

#define BIT_READ(value, bit) (((value) >> (bit)) & 0x01)
#define BIT_SET(value, bit) ((value) |= (1UL << (bit)))
#define BIT_CLEAR(value, bit) ((value) &= ~(1UL << (bit)))
#define BIT_WRITE(value, bit, bitvalue) (bitvalue ? BIT_SET(value, bit) : BIT_CLEAR(value, bit))

// #define DEBUG
#ifdef DEBUG
#define DB_DUMP_TO_PIN(data) dumpToPin(data)
#define DB_BIT_SET(value, bit) BIT_SET(value, bit)
#define DB_BIT_CLEAR(value, bit) BIT_CLEAR(value, bit)
#define DB_PULSE(value, bit) \
    BIT_SET(value, bit);     \
    BIT_CLEAR(value, bit)
#else
#define DB_DUMP_TO_PIN(data)
#define DB_BIT_SET(value, bit)
#define DB_BIT_CLEAR(value, bit)
#define DB_PULSE(value, bit)
#endif
#define NOP __asm__ __volatile__("nop\n\t")

typedef unsigned char uint8_t;

#define delayMicroseconds _delay_us

// OW states
#define IDLE 0x00
#define RX 0x10
#define RXROMCMD 0x10
#define RXROMCODE 0x11
#define RXDEVCMD 0x12
#define TX 0x20
#define TXROMCODE 0x21
#define TXTOERROR 0x22

// OW ROM Commands
#define OW_ROM_READ 0x33
#define OW_ROM_MATCH 0x55
#define OW_ROM_SKIP 0xCC
#define OW_ROM_RESUME 0xA5

// Joint Position Sensor:

#define OW_PIVOT_REPORT_SENSOR 0x59 // Report QRE1113 state - through trip and release
#define OW_PIVOT_GET_TO_ERROR 0x99 // Get QRE1113 timeout error

// Common:

#define OW_LED_OFF 0xC3 // LED OFF
#define OW_LED_ON 0xC6 // LED ON

// OW pin-related constants
#define OW_PORTX PORTB // output
#define OW_PINX PINB // input
#define OW_DDRX DDRB // direction: 1 = output, 0 = input
#define OW_PIN 2 // PB2 / INT0
#define OW_MASK _BV(OW_PIN)
#define OW_IS_HIGH (OW_PINX & OW_MASK)
#define OW_IS_LOW (!OW_IS_HIGH)
#define OW_ASSERT BIT_SET(OW_DDRX, OW_PIN) // OW_DDRX |= OW_MASK; // BIT_CLEAR(PORTB, 1) // set to output - should already be low
#define OW_RELEASE BIT_CLEAR(OW_DDRX, OW_PIN) // OW_DDRX &= ~OW_MASK; // BIT_SET(PORTB, 1) // set to input to tri-state and allow external pull-up to return line to high

#define TCRESET 255 // 450 µS - anything over 255 will be considered a reset
#define TCERROR 240 // 240 µS

void owTxBytes(uint8_t* buf, uint8_t len, uint8_t newState = TX);
void owRxBytes(uint8_t len, uint8_t newState);
void owByteRecd();
uint8_t crc8(uint8_t inData, uint8_t crc=0);
uint8_t crc8(uint8_t* inPtr, uint8_t len);
void dumpToPin(uint8_t data);

uint8_t owState; // current state
uint8_t owBitCount; // bit count down counter
uint8_t owByteCount; // byte count down counter
uint8_t owByte; // in- or out-bound byte
uint8_t owBuf[8]; // buffer for ROM code or up to 8 bytes of data
uint8_t owBufIndex; // pointer to current buffer byte
uint8_t owCRC;
bool resumeFlag;
bool followError;

const uint8_t owRomCode[8] = { 0x77, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60 }; // Family code: 77
const uint8_t owReply0[2] = { 0x00, 0x00 };
const uint8_t owReply255[2] = { 0xFF, 0x35 };

//                +--\/--+
//      (LED) PB0 | 1  8 | PB3 (Reset)
//            GND | 2  7 | VCC
//   (Sensor) PB1 | 3  6 | PB2 (INT0/OW)
//                +------+

#define QRE_PIN 1

#define LED_PIN 0
#define LED_ON BIT_SET(PORTB, LED_PIN)
#define LED_OFF BIT_CLEAR(PORTB, LED_PIN)

int main()
{
    OSCCAL = 0x7A;
    CCP = 0xD8; // Unlock protected registers
    CLKPSR = 0x00; // Set system clock back to 8 MHz

    DDRB = _BV(LED_PIN); // PB0 as an output
    PORTB = _BV(LED_PIN);
    PUEB = _BV(QRE_PIN);

    TCCR0A = _BV(WGM00); // Set timer to 8-bit mode
    TCCR0B = _BV(WGM02); // Set timer to 8-bit mode

	// ADMUX = _BV(MUX0); // Select ADC1 channel
	// DIDR0 = _BV(ADC1D); // ADC1 Digital Input Disable

    // BIT_SET(TCCR0B, CS01); // set Timer/Counter0 prescaler = 8 (1.0 MHz @ 8 MHz system clock); // start timer

    owState = IDLE;
    OW_RELEASE;

    PORTB = 0;

    for(;;) {
       	while(OW_IS_HIGH) ; // Wait for the line to go low
//*
		// DB_PULSE(PORTB, 1);

		// one execution per reset or bit

		TCNT0L = 0; // reset timer
		TIFR0 = _BV(TOV0); // Timer/Counter0 Overflow Flag - set to 1 to clear; // clear Timer/Counter Overflow Flag
		BIT_SET(TCCR0B, CS01); // set Timer/Counter0 prescaler = 8 (1.0 MHz @ 8 MHz system clock); // start timer

		// DB_PULSE(PORTB, 1);
//*
		if (owState & RX) {
			delayMicroseconds(25); // wait before testing line
			DB_PULSE(PORTB, 0);
			owByte >>= 1;
			if (OW_IS_HIGH) {
				DB_PULSE(PORTB, 0);
				owByte |= 0x80;
			}
			owBitCount -= 1;
			if (owBitCount == 0) {
				owByteRecd();
			}
			while (OW_IS_LOW) { // wait here for line to be released
				if (TCNT0L > TCERROR) { // should not have taken this long - something is wrong
					owState = IDLE;
					break;
				}
			}
		} else if (owState & TX) {
			DB_PULSE(PORTB, 1);
			if ((owByte & 0x01) == 0) {
				OW_ASSERT;
				delayMicroseconds(45);
				OW_RELEASE;
			}
			owByte >>= 1;
			owBitCount -= 1;
			if (owBitCount == 0) {
				owByteCount -= 1;
				if (owByteCount == 0) {
					owRxBytes(1, RXDEVCMD); // After sending the last byte assume the master still wants to talk to us so prepare to receive the next command.
				} else {
					owBufIndex += 1;
					owByte = owBuf[owBufIndex];
					owBitCount = 8;
				}
			}
			while (OW_IS_LOW) { // wait here for line to be released
				if (TCNT0L > TCERROR) { // should not have taken this long - something is wrong
					owState = IDLE;
					break;
				}
			}
		}

		if (owState == IDLE) {
			DB_PULSE(PORTB, 1);
			while (OW_IS_LOW) { // wait here for line to be released
				if (BIT_READ(TIFR0, TOV0)) { // timer overflow?
					BIT_CLEAR(TCCR0B, CS01); // stop timer
					TCNT0L = 0xFF; // set timer to MAX
					BIT_SET(TIFR0, TOV0); // Timer/Counter0 Overflow Flag - set to 1 to clear;
				}
			}
			if (TCNT0L == TCRESET) { // this was a reset pulse
				delayMicroseconds(20);
				OW_ASSERT; // let the master know we are here
				delayMicroseconds(80);
				OW_RELEASE;
				while (OW_IS_LOW) ; // wait here for line to be released as it may have been pulled low by another node
				owRxBytes(1, RXROMCMD); // expect the next byte to be a ROM command
			}
		}
	}

    return 0;
}

void owTxBytes(uint8_t buf[], uint8_t len, uint8_t newState)
{
    owByteCount = len;
    owBitCount = 8;
    owBufIndex = 0;
	for(uint8_t i = 0; i < len; i++) owBuf[i] = buf[i];
    owByte = owBuf[0];
    owState = newState;
}

void owRxBytes(uint8_t len, uint8_t newState)
{
    owByteCount = len;
    owBitCount = 8;
    owByte = 0;
    owBufIndex = 0;
    owState = newState;
    owCRC = 0;
}

void owByteRecd()
{
    if (owState == RXROMCMD) {
        switch(owByte) {
        case OW_ROM_READ:
            owTxBytes(owRomCode, 8, TXROMCODE);
            resumeFlag = false;
            break;
        case OW_ROM_MATCH:
            owRxBytes(8, RXROMCODE);
            resumeFlag = false;
            break;
        case OW_ROM_SKIP:
            owRxBytes(1, RXDEVCMD);
            resumeFlag = false;
            break;
        case OW_ROM_RESUME:
            if (resumeFlag)
                owRxBytes(1, RXDEVCMD);
            else
                owState = IDLE;
        }
    } else if (owState == RXDEVCMD) {
		DB_DUMP_TO_PIN(owByte);
		// Do something useful here
        switch (owByte) {
        case OW_LED_OFF: // turn off LED
            LED_OFF;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_LED_ON: // turn on LED
            LED_ON;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_PIVOT_REPORT_SENSOR: // Get sensor value
        	//ADCSRA = _BV(ADEN) | _BV(ADSC); // ADC Enable & ADC Start Conversion
			//while(!BIT_READ(ADCSRA, ADIF)) ;
        	//ADCSRA = BV(ADIF); // ADC Disable & clear ADC Interrupt Flag
            //owBuf[0] = BITREAD(PINB, 1) ? 0 : 255;
            //owBuf[1] = owBuf[0] ? 0x00 : 0x35;
            // Send value
            //owTxBytes(owBuf, 2);

			LED_ON;
            // QRE1113 is active low
			uint8_t count = 0;
            while (true) { // Waiting for QRE1113 to trip
				if(!BIT_READ(PINB, QRE_PIN)) count++;
				else count = 0;
				if(count == 4) break;
			}
			OW_ASSERT;
			count = 0;
			while (true) { // Waiting for QRE1113 to release
				if(BIT_READ(PINB, QRE_PIN)) count++;
				else count = 0;
				if(count == 4) break;
			}
			OW_RELEASE;
			LED_OFF;

            owRxBytes(1, RXDEVCMD);

            break;
//         case OW_PIVOT_GET_TO_ERROR: // Get timeout error
//             if(followError) owTxBytes(owReply255, 2, TXTOERROR);
//             else owTxBytes(owReply0, 2, TXTOERROR);
//             break;
        default:
            owRxBytes(1, RXDEVCMD);
        }
    } else { // All states where we are receiving multiple bytes
        owBuf[owBufIndex] = owByte;
        owCRC = crc8(owByte, owCRC);
        owByteCount -= 1;
        if (owByteCount > 0) { // more bytes to receive
            owBitCount = 8; // reset the bit count
            owByte = 0; // and owByte to prepare to receive the next byte
            owBufIndex += 1;
        } else {
            if (owCRC == 0) { // Valid data received
                if (owState == RXROMCODE) {
					uint8_t match = 1;
                    for(uint8_t i = 0; i < 8; i++) {
                        if(owBuf[i] != owRomCode[i]) {
                            match = 0;
                            break;
                        }
                    }
                    if (match) { // Did we receive our ROM code?
                        owRxBytes(1, RXDEVCMD); // If so, expect the next byte to be a command.
                        resumeFlag = true;
                    } else { //
                        owState = IDLE; // If not, go back to idle.
                    }
                }
            } else { // Data received is not valid
                owState = IDLE; // Go back to idle.
            }
        }
    }
}

const uint8_t CRC8_table[256] = {
    0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
    0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
    0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
    0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
    0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
    0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
    0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
    0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
    0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
    0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
    0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
    0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
    0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
    0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
    0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
    0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35
};

uint8_t crc8(uint8_t inData, uint8_t crc)
{
    return CRC8_table[(inData ^ crc) & 0xFF];
}

uint8_t crc8(uint8_t* inPtr, uint8_t len)
{
    uint8_t crc = 0;
    for (uint8_t i = 0; i < len; i++) {
        crc = CRC8_table[(*inPtr ^ crc) & 0xFF];
        inPtr += 1;
    }
    return crc;
}

void dumpToPin(uint8_t data) {
    BIT_CLEAR(PORTB, 1);
    NOP;
    NOP;

    BIT_SET(PORTB, 1);
    if (data & 0x80) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x40) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x20) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x10) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x08) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x04) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x02) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);

    BIT_SET(PORTB, 1);
    if (data & 0x01) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(PORTB, 1);
}
